<?php


class Station 
{
    public $x;
    public $y;
    public $z;
    
    function __construct($x,$y,$z){
        $this->x = $x;
        $this->y = $y;
        $this->z = $z;
    }

    public function distance($station){
        $val = 0;

        $diffx = $station->x - $this->x;
        $diffy = $station->y - $this->y;
        $diffz = $station->z - $this->z;

        $val += $this->square($diffx);
        $val += $this->square($diffy);
        $val += $this->square($diffz);
        echo $diffx.' '.$diffy.' '.$diffz.' '.PHP_EOL;
        echo floor(sqrt($val)).PHP_EOL;
        return floor(sqrt($val));
    }

    function square($val){
        return pow($val,2);
    }

    public function __toString()
    {
        return $this->x.' '.$this->y.' '.$this->z;
    }
}

$listStationsData = explode(PHP_EOL,file_get_contents('input1.txt'));

$stations = []; 

foreach ($listStationsData as $stationData) {
    $points     = explode(' ',$stationData);
    $station    = new Station((int)$points[0],(int)$points[1],(int)$points[2]);
    $stations[] = $station; 
}

usort($stations, function($a,$b){
    return $a->y >= $b->y; 
});

$distanceStation = 0;

for ($i=0; $i < count($stations) - 1 ; $i++) { 
    $stationA = $stations[$i];
    $stationB = $stations[$i+1];
    
    echo $stationA.PHP_EOL;
    echo $stationB.PHP_EOL;
    // $distanceAB = $stationA->distance($stationB);
    $distanceStation += $stationA->distance($stationB) ;
    // echo $distanceAB.PHP_EOL;
    echo PHP_EOL;
}

usort($stations, function($a,$b){
    return $a->y <= $b->y; 
});

echo '-----'.PHP_EOL;

for ($i=0; $i < count($stations) - 1 ; $i++) { 
    $stationA = $stations[$i];
    $stationB = $stations[$i+1];
    echo $stationA.PHP_EOL;
    echo $stationB.PHP_EOL;
    // $distanceAB = $stationA->distance($stationB);
    $distanceStation += $stationA->distance($stationB);
    // $distanceStation += $stationA->distance($stationB);

    // echo $distanceAB.PHP_EOL;
    echo PHP_EOL;
}

// for ($i=0; $i < count($stations) - 1 ; $i++) { 
//     $stationA = $stations[$i];
//     $stationB = $stations[$i+1];
//     $distanceStation += floor($stationA->distance($stationB));
// }

var_dump($distanceStation);