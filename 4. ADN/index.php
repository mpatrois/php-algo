<?php

$fragments = explode(PHP_EOL, file_get_contents('input6.txt'));
$nbFragments = array_shift($fragments);
$fullSequence = implode('',$fragments);
$lengthBrin = strlen($fullSequence)/2; 

usort($fragments, function($a,$b){
    return strlen($a) > strlen($b); 
});

function toComplementaire($frag){

    $complement = [
        'A' => 'T',
        'T' => 'A',
        'C' => 'G',
        'G' => 'C',
    ];

    $compl = '';
    for ($i = 0; $i < strlen($frag); $i++){
       $compl.= $complement[$frag[$i]];
    }
    return $compl;
}

function depth_picker($arr, $temp_string, &$collect) {
    if ($temp_string != "") 
        $collect []= $temp_string;

    for ($i=0; $i<sizeof($arr);$i++) {
        $arrcopy = $arr;
        $elem = array_splice($arrcopy, $i, 1); // removes and returns the i'th element
        if (sizeof($arrcopy) > 0) {
            depth_picker($arrcopy, $temp_string ." " . $elem[0], $collect);
        } else {
            $collect []= $temp_string. " " . $elem[0];
        }   
    }   
}

$collect = array();
depth_picker($fragments, "", $collect);



// var_dump($fullSequence);
// var_dump($lengthBrin);


$collect = array_filter($collect,function ($var) use($lengthBrin){
    $val = str_replace(' ', '', $var);
    return strlen($val) == $lengthBrin;
});

// var_dump($collect);

$results = [];
foreach ($collect as $brin1) {
    foreach ($collect as $brin2) {
        
        $valBrin1 = str_replace(' ','',$brin1);
        $valBrin2 = str_replace(' ','',$brin2);

        if($valBrin1 != $valBrin2){
            if( toComplementaire($valBrin1) == $valBrin2 ){
                $results[] = $brin1.'#'.$brin2;
            }
        }
    }
}
var_dump($results);