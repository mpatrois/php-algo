<?php

const puissance_arroseur = 1;

$lignes = explode(PHP_EOL, file_get_contents('input1.txt'));
$cases= [];
$nbCasesArrosee = 0;

foreach ($lignes as $ligne) {
    $cases[] = str_split($ligne);
}

for ($x=0; $x < count($cases) ; $x++) { 
    for ($y=0; $y < count($cases[$x]) ; $y++) {
        if(isBeenWater($cases,$x,$y)){
            print('A');
            $nbCasesArrosee++;
        }else {
            print($cases[$x][$y]) ;
        }
    }
    echo(PHP_EOL);
}

function isBeenWater($cases,$x,$y){
    for ($i = $x-puissance_arroseur; $i <= $x+puissance_arroseur ; $i++) { 
        for ($j = $y - puissance_arroseur ; $j <= $y+puissance_arroseur ; $j++) { 
            if( isset($cases[$i][$j]) && $cases[$i][$j] == 'X' && $cases[$x][$y]!="X"){
                return true;
            }
        }
    }
    return false;
}

print($nbCasesArrosee);