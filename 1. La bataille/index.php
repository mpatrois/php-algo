<?php

$tours = explode(PHP_EOL, file_get_contents('input1.txt'));
$nbTours = array_shift($tours);

$scorePlayerA = 0;
$scorePlayerB = 0;

foreach ($tours as $tour) {
    $carteA = explode(' ',$tour)[0];
    $carteB = explode(' ',$tour)[1];

    $scorePlayerA += $carteA > $carteB ? 1 : 0;
    $scorePlayerB += $carteA < $carteB ? 1 : 0;

}

if($scorePlayerA > $scorePlayerB){
    var_dump('A gagnant');
}else if($scorePlayerA < $scorePlayerB){
    var_dump('B gagnant');
}else{
    var_dump('egalité');
}